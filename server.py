from flask import Flask, render_template, redirect
from random import randint, sample

app = Flask(__name__)

### CONST

"""
You have to make sure that these are setup well according to this calculation:
    MAX_NUMBER - MIN_NUMBER > MAX_ROWS * MAX_ROWS / 2
"""

# The smallest number that may appear on a card
MIN_NUMBER = 1 

# The largest number that may appear on a card
MAX_NUMBER = 99

# The size of the largest board
MAX_ROWS = 12


### UTILS

def generate_board(rows:int):
    """Generates a board for the game that is populated with random number-pairs (or card pairs) and is formatted and
    returned as a list of lists

    Args:
        rows (int): the number of rows and cols for the board. One parameter is enough since the board
            always has the same number of rows and cols

    Returns:
        list: Returns a list of lists where each item is a row on the board
    """    

    # Making sure 
    if rows > MAX_ROWS:
        rows = MAX_ROWS

    # Generating the cards for the board
    cards = sample(range(MIN_NUMBER, MAX_NUMBER+1), int(rows*rows/2))*2
    
    # Filling the board randomly with the generated cards
    board = []
    for row in range(0, rows):
        new_row = []
        for cols in range(0, rows):
            new_row.append(cards.pop(randint(0, len(cards)-1)))
        board.append(new_row)
    return board


### ROUTING

# Index
@app.route('/')
def index():
    return render_template('index.html')

# Game / board
@app.route('/<int:rows>x<int:cols>')
def board(rows, cols):
    if rows != cols or rows*cols/2 != int(rows*cols/2):
        return redirect('/')
    
    return render_template('board.html',
        board=generate_board(int(rows)))


### MAIN LOOP

if __name__ == "__main__":
    app.run()