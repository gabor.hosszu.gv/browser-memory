$(document).ready(function() {

    // object that stores the selection
    var selection = {first : null, second : null}

    // this handles the lockdown of the game durring "animation"
    var locked = false;

    // number of matches found
    var matches = 0; 
    
    // number of matches left
    var rows = $("#board").children("tbody").children("tr").length;
    var matchesLeft = rows*rows/2; 
    $("#left").text(matchesLeft);

    // required for final game time
    var startDate = 0

    
    /*
    Click functions
    */

    // This is the main eventhandler that controls the game
    $("td").on("click", function() {
        if (!$(this).hasClass("card_match") && !$(this).hasClass("unturned")) {
            if (!locked) {
                $(this).addClass("unturned");
                if (selection.first == null) {
                    selection.first = $(this);
                } else {
                    selection.second = $(this);
                    locked = true;
                    if (selection.first.attr("data-value") == selection.second.attr("data-value")) {
                        markMatch()
                    } else {
                        setTimeout(resetSelection, 750)
                    }
                }
            }
        }
    })

    $("#restart").on("click", function() {
        location.reload();
    })

    $("#start").on("click", function() {
        $("#game").css("visibility", "visible");
        $(this).css("display", "none");
        startDate = Date.now();
    })

    /*
    Util functions
    */

    // Clears select object
    function resetSelection() {
        selection.first.removeClass("unturned");
        selection.first = null;
        selection.second.removeClass("unturned");
        selection.second = null;
        locked = false;
    }

    // Clears 
    function markMatch() {
        selection.first.addClass("card_match");
        selection.first = null;
        selection.second.addClass("card_match");
        selection.second = null;
        updateMatches();
        locked = false;
    }

    // Update scores and UI
    function updateMatches() {
        matches += 1;
        matchesLeft -= 1;
        $("#matches").text(matches);
        $("#left").text(matchesLeft);
        if (matchesLeft == 0) {
            $("#victory").css("visibility", "visible");
            $("#time").text("Időd: " + ((Date.now() - startDate)/1000).toFixed(2) + " mp");
            $("#time").css("visibility", "visible");
            $("#restart").css("display", "block");
        }
    }
})